resource "aws_instance" "web" {
  ami           = "ami-0b7937aeb16a7eb94"
  instance_type = "t3.nano"

  key_name = "tpt-b316"

  tags = {
    Name = "MINU SUPER AWESOME SOMETHING BLAAAAAA"
  }

  user_data = file("${path.module}/ec2-setup.sh")
}

output "web_id" {
  value = aws_instance.web.id
}


output "web_ip" {
  value = aws_instance.web.public_ip
}
